package theglobal

import (
	"os"
	//	"github.com/keratin/authn-go/authn"
)

var (
	CONSUL_ADDRESS = os.Getenv("CONSULADDRESS")
	CONSUL_KEY     = os.Getenv("CONSULKEY")
)

// AppConfigurationSettingsTemplate the template application configuration settings
type AppConfigurationSettingsTemplate struct {
	DB_HOST                string
	DB_PORT                string
	DB_USER                string
	DB_PASSWORD            string
	DB_NAME                string
	DB_SSLMODE             string
	AUTHN_URL              string
	AUTHN_PRIVATE_BASE_URL string
	AUTHN_USERNAME         string
	AUTHN_PASSWORD         string
	AUTHN_AUDIENCE_DOMAIN  string
	REDIS_ADDRESS          string
	REDIS_DBNUMBER         string
	CACHE_ENTRY_PREFIX     string
	USERNAME               string
	PASSWORD               string
	//	THESTORAGEAPP_ARCHIVE             string
	//	THESTORAGEAPP_ARCHIVE_DBNUMBER    string
	//	THESTORAGEAPP_CACHE               string
	//	THESTORAGEAPP_CACHE_DBNUMBER      string
	//	THESTORAGEAPP_COLLECTION          string
	//	THESTORAGEAPP_COLLECTION_DBNUMBER string
	//	THESTORAGEAPP_GRAPH               string
	//	THESTORAGEAPP_GRAPH_DBNUMBER      string
}

// AppConfigurationSettings the application's Global configuration settings
var AppConfigurationSettings = new(AppConfigurationSettingsTemplate)

//var AuthnConfig = authn.Config{}

type ASite struct {
	SiteAuthor    string
	SiteName      string
	SiteURL       string
	SiteCopyright string
}

var Site = ASite{
	SiteAuthor:    "Mooi Op Het Web",
	SiteName:      "SaaS",
	SiteURL:       "#",
	SiteCopyright: "SaaS",
}
