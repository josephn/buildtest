package thefilesystem

import (
	"embed"
	"html/template"
	"io"
	"io/fs"
	"net/http"
	"sync"
)

//go:embed templates/html/*
var TemplateFS embed.FS

var (
	theHTTPTemplateFSSingleInstance http.FileSystem
	theMutexLock                    sync.Mutex
)

// ATemplateSet contains a pointer to a set of templates
type ATemplateSet struct {
	Templates *template.Template
}

func (theTemplateSet *ATemplateSet) Render(writer io.Writer, name string, data interface{}) error {
	return theTemplateSet.Templates.ExecuteTemplate(writer, name, data)
}

// ToGetTemplateFS returns the singleton instance of the Template FileSystem
func ToGetTemplateFS() http.FileSystem {

	// check -> mutexLock -> check pattern to create a singleton
	if theHTTPTemplateFSSingleInstance == nil {

		theMutexLock.Lock()
		defer theMutexLock.Unlock()

		// this second check/if statement is in case another go routine raced to already create the singleton
		if theHTTPTemplateFSSingleInstance == nil {

			//			theLogger.Info("theFilesystemHandler.ToGetTemplateFS: Creating new Template Filesystem instance. ")

			// remove directory prefix from default TemplateFS for view templates
			theStrippedTemplateFS, theTemplateFSDirPrefixError := fs.Sub(TemplateFS, "templates/html")
			if theTemplateFSDirPrefixError != nil {
				panic(theTemplateFSDirPrefixError)
			}

			theHTTPTemplateFSSingleInstance = http.FileSystem(http.FS(theStrippedTemplateFS))
		}

	} else {

		//		theLogger.Info("theFilesystemHandler.ToGetTemplateFS: A Template Filesystem instance already exists, will use existing. ")

		return theHTTPTemplateFSSingleInstance

	}

	return theHTTPTemplateFSSingleInstance

}
