module gitlab.com/josephn/simos-saas/cmd/webservice

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.3 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/shakahl/golang-vfstemplate v0.0.0-20190528235634-28e3de6588b4 // indirect
)
