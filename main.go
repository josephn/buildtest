package main

import (
	"embed"
	"io/fs"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"gitlab.com/josephn/simos-saas/cmd/webservice/handlerOf/pages"
)

var (

	//go:embed public
	staticFS embed.FS
)

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}

func main() {

	theRouter := chi.NewRouter()

	theRouter.Use(middleware.RequestID)
	theRouter.Use(middleware.RealIP)
	theRouter.Use(middleware.Logger)
	theRouter.Use(middleware.Recoverer)

	theRouter.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World SaaS"))
	})

	theRouter.Get("/s/dashboard", pages.DashboardHandler)

	theRouter.Mount("/users", usersResource{}.Routes())
	theRouter.Mount("/todos", todosResource{}.Routes())

	// set the static file system
	// first remove directory prefix from default staticFS for view templates
	staticfilesfs, theEmbedFSDirPrefixError := fs.Sub(staticFS, "public")
	if theEmbedFSDirPrefixError != nil {
		panic(theEmbedFSDirPrefixError)
	}
	theStaticHTTPFilesystem := http.FS(staticfilesfs)
	FileServer(theRouter, "/public", theStaticHTTPFilesystem)

	http.ListenAndServe(":3000", theRouter)
}
