package pages

import (
	"html/template"
	"net/http"
	"q"

	vfstemplate "github.com/shakahl/golang-vfstemplate"
	theFilesystem "gitlab.com/josephn/simos-saas/cmd/webservice/theFilesystem"
	theGlobal "gitlab.com/josephn/simos-saas/cmd/webservice/theGlobal"
)

var (
	thisSite = theGlobal.Site
)

// DashboardHandler handles all requests to the .../dashboard/* endpoint
func DashboardHandler(theResponseWriter http.ResponseWriter, theRequest *http.Request) {

	theHTTPTemplateFS := theFilesystem.ToGetTemplateFS()

	theDashboardTemplateSet := &theFilesystem.ATemplateSet{

		Templates: template.Must(vfstemplate.ParseGlob(theHTTPTemplateFS, nil, "ui/dashboard/*.tmpl")),
	}

	type AMenuTile struct {
		HasTileName        string
		HasTileIcon        string
		HasTileIconHTML    string
		HasTileColour      string
		HasTileHRef        string
		HasTileDescription string
	}

	type AMainDashboardMenu struct {
		HasTiles []AMenuTile
	}

	type ADashboardDataSet struct {
		Title         string
		PageName      string
		Site          theGlobal.ASite
		DashboardMenu AMainDashboardMenu
	}

	Contacts := AMenuTile{
		HasTileName:   "CONTACTEN",
		HasTileIcon:   "address-book",
		HasTileColour: "green",
	}
	Multimedia := AMenuTile{
		HasTileName:   "MULTIMEDIA",
		HasTileIcon:   "camera",
		HasTileColour: "purple",
	}
	Communication := AMenuTile{
		HasTileName:   "COMMUNICATIE",
		HasTileIcon:   "comments",
		HasTileColour: "pink",
	}
	Internet := AMenuTile{
		HasTileName:   "INTERNET",
		HasTileIcon:   "globe",
		HasTileColour: "red",
	}
	Documents := AMenuTile{
		HasTileName:   "KANTOOR",
		HasTileIcon:   "file-alt",
		HasTileColour: "blue",
	}
	Games := AMenuTile{
		HasTileName:   "SPELLETJES",
		HasTileIcon:   "gamepad",
		HasTileColour: "yellow",
	}

	TheDashboardDataSet := ADashboardDataSet{
		Title:    "Dashboard",
		PageName: "STARTSCHERM",
		Site:     thisSite,
		DashboardMenu: AMainDashboardMenu{
			HasTiles: []AMenuTile{
				Contacts,
				Multimedia,
				Communication,
				Internet,
				Documents,
				Games,
			},
		},
	}

	q.Q(TheDashboardDataSet)
	theDashboardTemplateSet.Render(theResponseWriter, "dashboard.page.tmpl", TheDashboardDataSet)

	return
}
